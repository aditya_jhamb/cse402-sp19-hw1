/* Use "console.log()" method to print outputs */

/* Part 1: Functions */

/* 'Traditional' style functions */
function foo() { console.log('foo was executed'); }
function plus2(x: number) { return x + 2; }

/* Anonymous functions -- we can assign them to a variable */
var bar = function() { console.log('bar was executed') };
var times2 = function(x: number) { return x * 2; };

/* We can call functions */
foo();
bar();
console.log(plus2(2));
console.log(times2(5));

var lst = [1,2,3];

/* We can pass functions as arguments to other functions */
lst.map(plus2);
lst.map(times2);
lst.map(function(x: number) { return x * 3});

/* We can declare an anonymous function and immediately execute it */
(function() { console.log('anonymous function was executed'); })();

console.log("=================================");

/* Part 2: Objects */

/* Javascript uses prototype style objects. Think of them as a chain 
 * of lookup tables. This object has two fields, and inherits other 
 * fields and methods from the Object object */
var simpleObj = { x: 1, y: 2 };

/* The toString() method is inherited. Print to see what is returned */
simpleObj.toString();

/* In JavaScript, functions are first-class objects. Therefore, we can 
 * declare a prototype object in this style: */
function Baz(x: number, y: number) {
    this.x = x;   /* 'this' refers to the local scope */
    this.y = y;
};

/* Outside of a closure, 'this' refers to the global scope */
this.x = 100;

/* We create an instance of the Baz object by using the 'new' keyword.
 * The 'new' keyword turns a function into an object constructor. */
var bazObj = new Baz(2, 5);

/* What should the following statements print? Uncomment to confirm
 * your answer. */
// console.log(bazObj.x);
// console.log(this.x);

/* After executing the Baz function, will the print statements generate
 * a different output? Uncomment to confirm your answer.
 * Remember: 'this' always refers to current execution context. */ 
Baz(10, 10);
// console.log(bazObj.x);
// console.log(this.x);

/* We can add new fields and methods to a object by adding them to 
 * the prototype */
Baz.prototype.z = 1;
Baz.prototype.stateXYZ = function() {
    console.log("x is " + this.x + ", y is " + this.y + " and z is " + this.z);
}

var bazObj2 = new Baz(7, 7);
bazObj2.stateXYZ();

/* bazObj was created before we updated Baz.prototype, what should
 * happen if you uncomment the following statement? */
// bazObj.stateXYZ();

// We can also override inherited fields and methods
console.log("Before override: " + bazObj.toString());
Baz.prototype.toString = function() { return "i am a baz"; };
console.log("After override: " + bazObj.toString());

console.log("=================================");

/* Part 3: Call chaining */

function Circle() {
    this.x;
    this.y;
    this.color;
}

// by returning this, we can call the objects methods in a chain
Circle.prototype.setX = function(num: number) {
    this.x = num;
    return this;
}
Circle.prototype.setY = function(num: number) {
    this.y = num;
    return this;
}
Circle.prototype.setColor = function(hex: number) {
    this.color = hex;
    return this;
}

// this is compact and readable
var c = new Circle().setX(5).setY(2).setColor("#000000");