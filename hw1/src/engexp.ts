export class EngExpError extends Error {}

export class EngExp {
    private flags: string = "m";
    private pattern: string = "";
    // you can add new fields here
    private nested: string[] = [];
    private nestingType: boolean[] = [];

    // don't change sanitize()
    private static sanitize(s: string | EngExp): string | EngExp {
        if (s instanceof EngExp)
            return s;
        else {
            return s.replace(/[^A-Za-z0-9_]/g, "\\$&");
        }
    }

    // don't change these either
    public toString(): string {
        return this.asRegExp().source;
    }
    public valueOf(): string {
        return this.asRegExp().source;
    }
    public withFlags(flags: string) {
        if (/[^gimuy]/g.test(flags))
            throw new EngExpError("invalid flags");
        this.flags = "".concat(...new Set(flags));
        return this;
    }

    // asRegExp() will always be called before using your EngExp as a RegExp,
    // or before converting it into a string (for example to include via ``
    // template literals in another EngExp). You might want to take advantage
    // of this to check for errors...
    public asRegExp(): RegExp {
        if (this.nested.length != 0) {
            throw new EngExpError("Mismatch in either nesting or captures");
        }
        return new RegExp(this.pattern, this.flags);
    }

    // There is a bug somewhere in this code... can you find it?

    public match(literal: string): EngExp {
        return this.then(literal);
    }

    public then(pattern: string | EngExp): EngExp {
        this.handleNesting(`(?:${EngExp.sanitize(pattern)})`);
        return this;
    }

    public startOfLine(): EngExp {
        this.handleNesting("^(?:");
        return this;
    }

    public endOfLine(): EngExp {
        this.handleNesting(")$");
        return this;
    }

    public zeroOrMore(pattern?: EngExp): EngExp {
        if (pattern)
            return this.then(pattern.zeroOrMore());
        else {
            if (this.nested.length != 0) {
                this.nested[this.nested.length - 1] = `(?:${this.nested[this.nested.length - 1]})*`;
                return this;
            }
            this.pattern = `(?:${this.pattern})*`;
            return this;
        }
    }

    public oneOrMore(pattern?: EngExp): EngExp {
        if (pattern)
            return this.then(pattern.oneOrMore());
        else {
            if (this.nested.length != 0) {
                this.nested[this.nested.length - 1] = `(?:${this.nested[this.nested.length - 1]})+`;
                return this;
            }
            this.pattern = `(?:${this.pattern})+`;
            return this;
        }
    }

    public optional(): EngExp {
        if (this.nested.length != 0) {
            this.nested[this.nested.length - 1] = `(?:${this.nested[this.nested.length - 1]})?`;
            return this;
        }
        this.pattern = `(?:${this.pattern})?`;
        return this;
    }

    public maybe(pattern: string | EngExp): EngExp {
        this.handleNesting(`(?:${EngExp.sanitize(pattern)})?`);
        return this;
    }

    public anythingBut(characters: string): EngExp {
        this.handleNesting(`[^${EngExp.sanitize(characters)}]*`);
        return this;
    }

    public digit(): EngExp {
        this.handleNesting("\\d");
        return this;
    }

    public repeated(from: number, to?: number): EngExp {
        if (to === undefined) {
            if (this.nested.length == 0) {
                this.pattern = `(?:${this.pattern}){${from}}`;
            } else {
                this.nested[this.nested.length - 1] = `(?:${this.nested[this.nested.length - 1]}){${from}}`;
            }
        } else {
            if (this.nested.length == 0) {
                this.pattern = `(?:${this.pattern}){${from},${to}}`;
            } else {
                this.nested[this.nested.length - 1] = `(?:${this.nested[this.nested.length - 1]}){${from},${to}}`;
            }
        }
        return this;
    }

    public multiple(pattern: string | EngExp, from: number, to?: number) {
        if (to === undefined) {
            this.handleNesting(`(?:${EngExp.sanitize(pattern)}){${from}}`);
        } else {
            this.handleNesting(`(?:${EngExp.sanitize(pattern)}){${from},${to}}`);
        }
        return this;
    }

    // you need to implement these five operators:

    public or(pattern: string | EngExp): EngExp {
        // FILL IN HERE
        this.handleNesting(`|(?:${EngExp.sanitize(pattern)})`);
        return this;
    }

    public beginLevel(): EngExp {
        // FILL IN HERE
        this.nested.push('(?:');
        this.nestingType.push(false);
        return this;
    }

    public endLevel(): EngExp {
        // FILL IN HERE
        if (this.nestingType.length == 0 || this.nestingType[this.nestingType.length - 1]) {
            throw new EngExpError("Level Mismatch");
        }
        let lastLevel : string = this.nested.pop() + `)`;
        this.nestingType.pop();
        this.handleNesting(lastLevel);
        return this;
    }

    public beginCapture(): EngExp {
        // FILL IN HERE
        this.nested.push('(');
        this.nestingType.push(true);
        return this;
    }

    public endCapture(): EngExp {
        // FILL IN HERE
        if (this.nestingType.length == 0 || !this.nestingType[this.nestingType.length - 1]) {
            throw new EngExpError("Capture Mismatch");
        }
        let lastLevel : string = this.nested.pop() + `)`;
        this.handleNesting(lastLevel);
        this.nestingType.pop();
        return this;
    }

    private handleNesting(str: string) {
        if (this.nested.length != 0) {
            this.nested[this.nested.length - 1] += str;
        } else {
            this.pattern += str;
        }
    }
}

// ---- EXTRA CREDIT: named capture groups ----

// You'll need to modify a few parts of your EngExp implementation.
// First, change the signatures of beginCapture() and endCapture() to the following:
//
//     beginCapture(name?: string): EngExp
//     endCapture(name?: string): EngExp
//
// JavaScript regular expressions don't support named groups out of the box,
// so we'll need to extend them. Change the signature of asRegExp() to the following:
//
//     asRegExp(): NamedRegExp
//
// The interface for NamedRegExp is provided below (don't change it!) and depends on
// the new interface for NamedRegExpExecArray.

interface NamedRegExpExecArray extends RegExpExecArray {
    groups: Map<string, string | undefined>;
}
interface NamedRegExp extends RegExp {
    exec(s: string): NamedRegExpExecArray | null;
}

// Essentailly, a NamedRegExp is like a RegExp, but when you call its exec() method
// you get back an array with an extra field, groups, which is a hashmap from the
// names of capture groups to what those groups matched. You can access it like this:
//
//     let r = new EngExp().beginCapture("justf").then("f").endCapture().then("oo").asRegExp()
//     r.exec("foo").groups["justf"]  ==  "f"
//
// See the tests at the end of test/engexp.ts for more exampls of usage.
