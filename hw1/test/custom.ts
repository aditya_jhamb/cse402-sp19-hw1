import {expect} from "chai";
import {EngExp, EngExpError} from "../src/engexp";

describe("custom", () => {

    // Tests are created with the it() function, which takes a description
    // and the test body, wrapped up in a function.
    it("should parse multi-level nesting", () => {

        // put the body of the test here
        const e = new EngExp().match("a")
            .beginLevel()
                .then("A").or("BC")
                .beginLevel()
                    .maybe("DEF")
                    .then("xyz")
                    .beginLevel()
                        .anythingBut("/")
                        .then(".co")
                        .beginLevel()
                            .then("m")
                            .or(".")
                            .beginLevel()
                                .then("in")
                                .or("uk")
                                .or("ca")
                            .endLevel()
                        .endLevel()
                    .endLevel()
                .endLevel()
            .endLevel()
            .asRegExp();

        const validStr1 : string = "aBCDEFxyz.co.in";
        const validStr2 : string = "jkwnkjktaBCDEFxyz.co.cajn3lnj234";
        const invalidStr1 : string = "";

        expect(e.test(validStr1)).to.be.true;
        expect(e.test(validStr2)).to.be.true;
        expect(e.test(invalidStr1)).to.be.false;

        expect(e.exec(validStr1).length).to.be.equal(1);
        expect(e.exec(validStr2).length).to.be.equal(1);
        expect(e.exec(invalidStr1)).to.be.null;
    });

    // You should modify the above test so it's interesting, and provide
    // at least 4 more nontrivial tests.

    it("should parse multi-nested capturing", () => {

        // put the body of the test here
        const e = new EngExp().match("a")
            .beginCapture()
                .then("A").or("BC")
                .beginCapture()
                    .maybe("DEF")
                    .then("xyz")
                    .beginCapture()
                        .anythingBut("/")
                        .then(".co")
                        .beginCapture()
                            .then("m")
                            .or(".")
                            .beginCapture()
                                .then("in")
                                .or("uk")
                                .or("ca")
                            .endCapture()
                        .endCapture()
                    .endCapture()
                .endCapture()
            .endCapture()
            .asRegExp();

        const validStr1 : string = "aBCDEFxyz.co.in";
        const validStr2 : string = "jkwnkjktaBCDEFxyz.co.cajn3lnj234";
        const invalidStr1 : string = "";

        expect(e.test(validStr1)).to.be.true;
        expect(e.test(validStr2)).to.be.true;
        expect(e.test(invalidStr1)).to.be.false;

        const result1 = e.exec(validStr1);
        const result2 = e.exec(validStr2);

        expect(result1.length).to.be.equal(6);
        expect(result1[0]).to.be.equal(validStr1);
        expect(result1[4]).to.be.equal(".in");

        expect(result2.length).to.be.equal(6);
        expect(result2[0]).to.be.equal("aBCDEFxyz.co.ca");
        expect(result2[4]).to.be.equal(".ca");
    });

    it('should handle mismatch in nested levels', () => {
        function f1() {
            new EngExp().beginLevel().endLevel().endLevel().asRegExp();
        }

        function f2() {
            new EngExp().endLevel().endLevel().asRegExp();
        }

        function f3() {
            new EngExp().beginLevel().asRegExp();
        }

        function f4() {
            new EngExp().beginLevel().then(
                new EngExp().endLevel()
            );
        }

        expect(f1).to.throw(EngExpError);
        expect(f2).to.throw(EngExpError);
        expect(f3).to.throw(EngExpError);
        expect(f4).to.throw(EngExpError);
    });

    it('should handle mismatch in nested captures', () => {
        function f1() {
            new EngExp().beginCapture().endCapture().endCapture().asRegExp();
        }

        function f2() {
            new EngExp().endCapture().endCapture().asRegExp();
        }

        function f3() {
            new EngExp().beginCapture().asRegExp();
        }

        // test case from Part 3
        function f4() {
            new EngExp().beginCapture().then(
                new EngExp().endCapture()
            );
        }

        expect(f1).to.throw(EngExpError);
        expect(f2).to.throw(EngExpError);
        expect(f3).to.throw(EngExpError);
        expect(f4).to.throw(EngExpError);
    });

    it("should handle mixed mismatch in nested levels and nested captures", () => {
       function f1() {
           new EngExp().beginLevel().endCapture().asRegExp();
       }

       function f2() {
           new EngExp().beginCapture().endLevel().asRegExp();
       }

       function f3() {
           new EngExp().beginLevel().beginCapture().endLevel().endLevel().endCapture().asRegExp();
       }

       expect(f1).to.throw(EngExpError);
       expect(f2).to.throw(EngExpError);
       expect(f3).to.throw(EngExpError);
    });

    it("should parse phone numbers", () => {
        function nDigits(n : number){
            return new EngExp().digit().repeated(n)
        }

        const threeDigits = nDigits(3);
        const fourDigits = nDigits(4);

        const e = new EngExp()
            .startOfLine()
                .then('(')
                .then(threeDigits)
                .then(')')
                .then(' ')
                .then(threeDigits)
                .then('-')
                .then(fourDigits)
                .or(nDigits(10))
            .endOfLine()
            .asRegExp();

        const validStr1 : string = "(123) 456-7890";
        const validStr2 : string = "1234567890";
        const invalidStr1: string = "(123) 573 0123";
        const invalidStr2: string = "(123)5730123";
        const invalidStr3: string = "112312325730123";

        expect(e.test(validStr1)).to.be.true;
        expect(e.test(validStr2)).to.be.true;
        expect(e.test(invalidStr1)).to.be.false;
        expect(e.test(invalidStr2)).to.be.false;
        expect(e.test(invalidStr3)).to.be.false;

        const result = e.exec(validStr1);
        const undef = e.exec(invalidStr1);

        expect(result[0]).to.be.equal("(123) 456-7890");
        expect(undef).to.be.null;
    })
});
