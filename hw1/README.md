This directory contains a starter kit for Homework 1, "Regular Expressions".

`engexp` is a Node.js module, written in [TypeScript](http://www.typescriptlang.org/), a typed
superset of JavaScript. The repository initially contains TypeScript original sources (\*.ts);
generate the executable JavaScript files (\*.js) with the TypeScript compiler `tsc`. The module
is configured to run the compiler for you automatically before you run the tests.

To get started after cloning this repository, install [Node.js](https://nodejs.org),
and run `npm install` in this directory (hw1/). After NPM successfully resolves all
dependencies into `npm_modules`, run `npm test`. You should see an output similar to the following:

```
> hw1@2019.1.0 pretest ~/csep402-sp19/hw1
> tsc

> hw1@2019.1.0 test ~/csep402-sp19/hw1
> mocha

  custom
    ✓ put description of test 1 here

  EngExp
    ✓ should handle invalid and duplicated flags
    1) should parse a basic URL
    ✓ should parse a URL with query and fragment
    2) match, then, zeroOrMore, oneOrMore, optional, and maybe should agree for string repeated exactly once
    3) should parse a disjunctive date pattern
    4) disjunctive date pattern with levels
    5) should capture nested groups
    6) should capture nested groups and work with disjunctions

  3 passing (14ms)
  6 failing

  1) EngExp
       should parse a basic URL:

      AssertionError: expected true to be false
      + expected - actual

      -true
      +false

      at Context.it (test/engexp.js:31:61)

  2) EngExp
       match, then, zeroOrMore, oneOrMore, optional, and maybe should agree for string repeated exactly once:

      AssertionError: expected false to be true
      + expected - actual

      -false
      +true

      at Context.it (test/engexp.js:119:47)

  3) EngExp
       should parse a disjunctive date pattern:
     Error: unimplemented
      at EngExp.or (src/engexp.js:99:15)
      at Context.it (test/engexp.js:124:27)

  4) EngExp
       disjunctive date pattern with levels:
     Error: unimplemented
      at EngExp.or (src/engexp.js:99:15)
      at Context.it (test/engexp.js:150:27)

  5) EngExp
       should capture nested groups:
     Error: unimplemented
      at EngExp.beginCapture (src/engexp.js:111:15)
      at Context.it (test/engexp.js:180:14)

  6) EngExp
       should capture nested groups and work with disjunctions:
     Error: unimplemented
      at EngExp.beginLevel (src/engexp.js:103:15)
      at Context.it (test/engexp.js:194:28)
```

Out of the eight tests defined in [test/engexp.ts](test/engexp.ts), only two are
currently passing. The first two failures are due to a bug in the starter kit - you should
fix that first to familiarize yourself with the code. Once you've done that, your goal is
to implement missing functions `or`, `beginLevel`, `endLevel`, `beginCapture`, and `endCapture`
in [src/engexp.ts](src/engexp.ts) so that the rest of the tests pass. You should also write
your own tests, in [test/custom.ts](test/custom.ts).

Note: we will be verifying your solution on an additional hidden suite of tests, so
please make sure your implementation is general-purpose.